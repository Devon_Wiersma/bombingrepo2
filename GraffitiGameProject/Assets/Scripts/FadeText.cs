using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FadeText : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI textToUse;
    [SerializeField] private bool fadeIn = false;
    [SerializeField] private bool fadeOnStart = false;
    [SerializeField] private float timeMultiplier;
    public CanvasGroup canGroup;
    private bool FadeIncomplete = false;
    public bool textActive = false;
    bool singletonText = true;
    public bool isBoombox;

    private void Start()
    {
        Transform playerTrans = GameObject.Find("First person camera").transform;
        canGroup = GetComponent<CanvasGroup>();
    }

    private void Update()
    {
        if (textActive)
        {
            if (singletonText)
            {
                canGroup.alpha = 1;
                singletonText = false;
            }

            if (canGroup.alpha > 0)
            {
                canGroup.alpha = canGroup.alpha - timeMultiplier;
            }
            else
            {
                textActive = false;
                singletonText = true;
            }
            if (!isBoombox)
                transform.Rotate(0, 1f, 0);
        }

    }


    public void DisplayText(string text, Color color)
    {
        singletonText = true;
        textActive = true;

        GetComponent<TextMeshProUGUI>().color = color;
        GetComponent<TextMeshProUGUI>().text = text;

    }
}