using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Michsky.UI.ModernUIPack;

using UnityEngine.Analytics;

public class LangResolver : MonoBehaviour
{
    private const char Separator = '=';
    [SerializeField] public readonly Dictionary<string, string> _lang = new Dictionary<string, string>();
    private SystemLanguage _language;

    public bool deleteSaveData;

    public static LangResolver instance;

    public UIManager uiManager;

    private TMP_FontAsset activeLanguageFont;

    public TMP_FontAsset SimplifiedChineseFont;
    public TMP_FontAsset TraditionalChineseFont;
    public TMP_FontAsset KoreanFont;
    public TMP_FontAsset JapaneseFont;

    public bool scaleText;

    public string[] lineArray;

    private void OnLevelWasLoaded(int level)
    {
        ReadProperties();
        ResolveTexts();
    }

    private void OnEnable()
    {
        ReadProperties();
        ResolveTexts();
    }

    private void Awake()
    {

        DontDestroyOnLoad(this);


        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        ReadProperties();
        ResolveTexts();
    }
    private void ReadProperties()
    {
        //_language = SystemLanguage.ChineseSimplified;
        //_language = SystemLanguage.ChineseTraditional;
        //_language = SystemLanguage.Portuguese;
        //_language = SystemLanguage.Japanese;
        _language = Application.systemLanguage;

        var file = Resources.Load<TextAsset>(_language.ToString());
        print(file.name);
        if (file == null)
        {
            file = Resources.Load<TextAsset>(SystemLanguage.English.ToString());
            _language = SystemLanguage.English;
        }
        int numIterations = 0;
        //split all the lines up
        foreach (var line in file.text.Split('\n'))
        {
            var prop = line.Split(Separator);

            _lang[prop[0]] = prop[1];
            numIterations = numIterations + 1;
            print(numIterations);
        }
    }

    public void ResolveTexts()
    {
        var allTexts = Resources.FindObjectsOfTypeAll<LangText>();
        CheckLanguage();
        //for every object in the scene
        foreach (var langText in allTexts)
        {
            print("Checking all langtext");
            //throws error if there's no key in the localizaiton matching the identifier
            if (!_lang.ContainsKey(langText.Identifier))
            {
                Debug.LogError("The assosciated localization key for " + langText.Identifier + " on object " + langText.gameObject.name + " cannot be found");
            }
            else
            {
                //gets string from localization key
                string propertyText = Regex.Unescape(_lang[langText.Identifier]);
                //print("Text of current accessed property (" + langText.gameObject.name + ") is: " + propertyText);

                if (propertyText == null)
                {
                    Debug.LogError("This language resolve got no Key back! Make sure there is a key logged or this LangText has a proper Identifier");
                }


                //gets variables for buttons
                if (langText.gameObject.GetComponent<ButtonManagerBasic>() != null)
                {
                    var text = langText.GetComponent<ButtonManagerBasic>();
                    text.buttonText = propertyText;
                    text.normalText.font = activeLanguageFont;
                    uiManager.buttonFont = activeLanguageFont;
                    text.UpdateUI();
                    //  print(langText.gameObject.name + " has been translated to " + propertyText);
                }

                //gets variables for dropdowns
                if (langText.gameObject.GetComponent<UIManagerDropdown>() != null)
                {
                    var text = langText.GetComponent<UIManagerDropdown>();


                    //text.buttonText = propertyText;
                    //text.normalText.font = activeLanguageFont;
                    //uiManager.buttonFont = activeLanguageFont;
                    //text.UpdateUI();
                    //  print(langText.gameObject.name + " has been translated to " + propertyText);
                }

                //gets variables for text objects
                if (langText.gameObject.GetComponent<TextMeshProUGUI>() != null)
                {
                    var text = langText.GetComponent<TextMeshProUGUI>();
                    text.font = activeLanguageFont;
                    text.text = propertyText;
                    //  print(langText.gameObject.name + " has been translated to " + propertyText);
                }

                //scale language up if required
                langText.ScaleText(_language);
            }
        }

    }

    //change font displayed based on active language
    void CheckLanguage()
    {
        if (_language == SystemLanguage.ChineseSimplified)
        {
            activeLanguageFont = SimplifiedChineseFont;
            scaleText = true;
        }
        if (_language == SystemLanguage.ChineseTraditional)
        {
            activeLanguageFont = TraditionalChineseFont;
        }
        if (_language == SystemLanguage.Korean)
        {
            activeLanguageFont = KoreanFont;
        }
        if (_language == SystemLanguage.Japanese)
        {
            activeLanguageFont = JapaneseFont;

        }



    }


}
