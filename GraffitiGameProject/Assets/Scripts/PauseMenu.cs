using Steamworks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    CameraPixelation cameraPixelation;
    FirstPersonLook fpLook;
    public Slider PixelationSlider;
    public GameObject pausePanel;
    public GameObject optionsPanel;
    public GameObject creditsPanel;
    GameObject itemBelt;
    public SceneLoader sceneLoader;
    public bool isMainMenu;
    public bool pauseVisible = false;
    public bool optionsVisible = false;
    public bool creditsVisible = false;

    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("First person camera") != null)
        {
            cameraPixelation = GameObject.Find("First person camera").GetComponent<CameraPixelation>();
           fpLook = GameObject.Find("First person camera").GetComponent<FirstPersonLook>();
        //    cameraPixelation.forcedPixellationValue = PlayerPrefs.GetInt("CamPixellation");
        //    if (cameraPixelation.h == 0)
        //    {
        //        cameraPixelation.h = 200;
        //    }
        //    PixelationSlider.value = cameraPixelation.forcedPixellationValue;
        }

        itemBelt = GameObject.Find("ItemBelt");
        sceneLoader = GameObject.Find("LoadScreenCanvas").GetComponent<SceneLoader>();
        if (!isMainMenu)
            pausePanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            PauseToggle();
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            DeletePlayerPrefs();
        }
    }

    public void PauseToggle()
    {
        //if the pause panel is already visible
        if (pausePanel.activeSelf == true)
        {
            pauseVisible = false;
            if (fpLook != null)
                fpLook.lockMovement = false;
            Time.timeScale = 1;
            pausePanel.SetActive(false);
            if (!isMainMenu)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
        //if the pause panel isn't visible
        else
        {
            pauseVisible = true;
            if (fpLook != null)
                fpLook.lockMovement = true;
            Time.timeScale = 0;
            pausePanel.SetActive(true);

            if (!isMainMenu)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        //enables options so it will be turned off again in function
        optionsVisible = true;
    }

    //toggles options on or off depending on the current state of the menu
    public void OptionsToggle()
    {
        if (!optionsVisible)
        {
            optionsPanel.SetActive(true);
            optionsVisible = true;
        }
        else
        {
            optionsPanel.SetActive(false);
            optionsVisible = false;
        }

    }

    public void CreditsToggle()
    {
        if (!creditsVisible)
        {
            creditsPanel.SetActive(true);
            creditsVisible = true;
        }
        else
        {
            creditsPanel.SetActive(false);
            creditsVisible = false;
        }

    }

    public void UpdatePixelSlider()
    {
        if (!isMainMenu)
        {
            cameraPixelation.UpdatePixelValue((int)PixelationSlider.value);
            //cameraPixelation.h = (int)PixelationSlider.value;
            //PlayerPrefs.SetInt("CamPixellation", cameraPixelation.forcedPixellationValue);
        }
        PlayerPrefs.Save();

    }

    public void SetQualitySetting(int qualityIndex)
    {

        QualitySettings.SetQualityLevel(qualityIndex);
        print("Graphics Setting Changed to " + QualitySettings.currentLevel);
        //qualityDropdown.value = qualityIndex;
    }

    public void SetFullscreenSetting(bool fullscreen)
    {
        if (fullscreen == true)
        {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        }

        if (fullscreen == false)
        {
            Screen.fullScreenMode = FullScreenMode.Windowed;
        }
    }

    //DEBUG clears player preferences
    public void DeletePlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        SteamUserStats.ResetAllStats(true);
    }

    public void ToggleSaveClearBool()
    {
        GameObject.Find("LangResolver").GetComponent<LangResolver>().deleteSaveData = true;
    }


    public void QuitGame()
    {
        Application.Quit();
    }
}
