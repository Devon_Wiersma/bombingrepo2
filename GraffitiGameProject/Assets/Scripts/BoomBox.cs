using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomBox : MonoBehaviour
{
    public AudioClip[] musicPlaylist;
    public int musicIndex = 0;
    bool playing = true;
    AudioSource audioSource;
    public FadeText boomBoxText;

    // Start is called before the first frame update
    void Start()
    {
        musicIndex = Random.Range(0, musicPlaylist.Length);
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = musicPlaylist[musicIndex];
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {

        if (!audioSource.isPlaying)
        {
            NextSong();
        }

    }

    public void ToggleBoombox()
    {
        if (playing)
        {
            playing = false;
            audioSource.volume = 0;
            boomBoxText.DisplayText("OFF", Color.white);
        }
        else
        {
            playing = true;
            audioSource.volume = 0.5f;
            boomBoxText.DisplayText("ON", Color.white);
        }
    }

    public void NextSong()
    {
        audioSource.Stop();
        //musicIndex = Random.Range(0, musicPlaylist.Length);
        if (musicIndex + 1 == musicPlaylist.Length)
        {
            musicIndex = 0;
        }
        else
        {
            ++musicIndex;
        }
        audioSource.PlayOneShot(musicPlaylist[musicIndex]);

        DisplaySongName();
    }

    public void DisplaySongName()
    {
        boomBoxText.DisplayText(musicPlaylist[musicIndex].name, Color.white);
    }


}
