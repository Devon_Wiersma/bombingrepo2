using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Manager : MonoBehaviour
{
    public bool colourBlindMode;
    public bool pauseMenuVisible;
    public GameObject pauseMenu;
    public GameObject loadScreen;

    float testvalue = 0;

    // Start is called before the first frame update
    void Start()
    {
        TurnOnColourblind();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            StartCoroutine("TakeScreenshot");
        }

    }

    public void IncrementPaintTime(float value)
    {
        testvalue += value;
        if (PlayerPrefs.HasKey(SceneManager.GetActiveScene().name + " spray value " + value))
        {

            //  PlayerPrefs.SetFloat()
        }
        else
        {
            PlayerPrefs.HasKey(SceneManager.GetActiveScene().ToString());
        }
        print(SceneManager.GetActiveScene().name + " spray value " + testvalue);
    }

    void TurnOnColourblind()
    {
        GameObject[] colourBlindObjects = GameObject.FindGameObjectsWithTag("ColourBlind");
        if (colourBlindMode)
        {
            for (int i = 0; i < colourBlindObjects.Length; i++)
            {
                colourBlindObjects[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < colourBlindObjects.Length; i++)
            {
                colourBlindObjects[i].SetActive(false);
            }
        }
    }

    IEnumerator TakeScreenshot()
    {
        //if the screenshot directory doesn't exist, create one
        if (!Directory.Exists(Application.persistentDataPath + "/screenshots/"))
            Directory.CreateDirectory(Application.persistentDataPath + "/screenshots/");


        GameObject[] RemoveOnScreenshotObjects = GameObject.FindGameObjectsWithTag("RemoveOnScreenshot");

        //disable aall objects tagged as "remove on screenshot"
        for (int i = 0; i < RemoveOnScreenshotObjects.Length; i++)
        {
            RemoveOnScreenshotObjects[i].SetActive(false);
        }

        //wait until frame ends
        yield return new WaitForEndOfFrame();

        //filepath with date
        ScreenCapture.CaptureScreenshot(Application.persistentDataPath + "/screenshots/" + System.DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png");

        //reenable objects
        for (int i = 0; i < RemoveOnScreenshotObjects.Length; i++)
        {
            RemoveOnScreenshotObjects[i].SetActive(true);
        }

        //print text for console
        GameObject consoleObject = GameObject.Find("ConsoleText");
        consoleObject.GetComponent<Text>().text = ("Screenshot Saved To: " + Application.persistentDataPath + "/screenshots/" + System.DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png");

        yield return new WaitForSeconds(8);

        consoleObject.GetComponent<Text>().text = null;
    }
}
