using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveCan : MonoBehaviour
{
    public float canCapacity = 100;
    public float canSprayRate;

    public CapManager capManager;
    public BeltHandler beltHandler;
    public Manager manager;

    public GameObject activeCanLabel;
    public GameObject spentCanPrefab;
    public PauseMenu pauseMenu;

    bool soundPlayingSingleton = false;

    private AudioSource audioSource;
    public AudioClip fullCanAudio;
    public AudioClip lowCanAudio;
    public AudioClip pickupCanAudio;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        pauseMenu = GameObject.Find("Pause Menu Holder").GetComponent<PauseMenu>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!pauseMenu.pauseVisible)
        {
            //depletes at can spray rate
            //defined in CapManager script
            if (gameObject.GetComponent<PaintIn3D.P3dToggleParticles>().isSpraying)
            {


                //actions for when the can is low on paint
                if (canCapacity <= 15)
                {
                    audioSource.clip = lowCanAudio;
                    audioSource.pitch = 1.5f;
                    capManager.canPaintIsLow = true;
                }
                else //For when the can is full
                {
                    audioSource.clip = fullCanAudio;
                    audioSource.pitch = 1f;
                    capManager.canPaintIsLow = false;
                }

                //play sound on first initialization of the looping SFX
                if (!soundPlayingSingleton)
                {
                    audioSource.loop = true;
                    audioSource.Play();
                    soundPlayingSingleton = true;
                }

                //Drain can at predefined rate set by cap
                canCapacity = canCapacity - (canSprayRate / 100);
                manager.IncrementPaintTime(canSprayRate);

            }
            else
            {
                soundPlayingSingleton = false;
                if (audioSource.isPlaying && audioSource.clip == fullCanAudio || audioSource.clip == lowCanAudio)
                {
                    audioSource.Stop();
                }
            }



            //dispose of can and color it to match previous active cand
            if (canCapacity <= 0)
            {
                CreateSpentCan();
                audioSource.Stop();
            }
        }
    }

    public void ResetCan()
    {
        canCapacity = 100;
        audioSource.clip = pickupCanAudio;
        audioSource.loop = false;
        audioSource.Play();
    }

    void CreateSpentCan()
    {
        beltHandler.canActive = false;
        GameObject _spentCan = null;
        _spentCan = Instantiate(spentCanPrefab, gameObject.transform.position, gameObject.transform.rotation);

        //cycle material and change colour based on name to emulate the disposed can
        MeshRenderer[] _mr = _spentCan.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < _mr.Length; i++)
        {
            if (_mr[i].gameObject.name == "Label")
            {

                _mr[i].material.color = activeCanLabel.GetComponent<MeshRenderer>().material.color;
                _mr[i].material = activeCanLabel.GetComponent<MeshRenderer>().material;
            }
        }

        _spentCan.GetComponent<Rigidbody>().AddForce(new Vector3(0, 2, 0), ForceMode.Impulse);

    }
}
