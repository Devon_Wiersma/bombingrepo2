using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CapManager : MonoBehaviour
{
    public GameObject playerCam;
    public GameObject canParticlesObj;
    public GameObject capCanvasObj;
    GameObject capHandlerObject;
    public GameObject capTextPrefab;
    public BeltHandler beltHandler;
    public FadeText newCapText;

    public string[] caps = { "Stock", "Skinny", "N.Y. Fat", "Firehose" };
    public float[] capSprayRate = { 0.004f, 0.001f, 0.1f, 1f };
    public GameObject[] capVisuals;
    public float particleEmissionOverTime;

    string activeCap;
    int activeCapIndex = 0;

    public bool canPaintIsLow;




    // Start is called before the first frame update
    void Start()
    {
        activeCap = caps[0];
    }

    // Update is called once per frame
    void Update()
    {
        //cycle up and down through array based on player input
        if (Input.GetButtonDown("CycleCapsUp"))
        {
            CycleCap("Up");
        }
        if (Input.GetButtonDown("CycleCapsDown"))
        {
            CycleCap("Down");
        }
        SetCapParameters();
        // print(activeCap);
    }

    void CycleCap(string direction)
    {
        capVisuals[activeCapIndex].SetActive(false);
        if (direction == "Up")
        {
            if (activeCapIndex == caps.Length - 1)
            {
                activeCapIndex = 0;
            }
            else
            {
                activeCapIndex++;
            }
        }
        if (direction == "Down")
        {
            if (activeCapIndex == 0)
            {
                activeCapIndex = caps.Length - 1;
            }
            else
            {
                activeCapIndex--;
            }
        }
        //cycle through visuals for cap as well as set all appropriate assets
        activeCap = caps[activeCapIndex];
        capVisuals[activeCapIndex].SetActive(true);

        newCapText.DisplayText(caps[activeCapIndex], Color.white);
        newCapText.GetComponent<LangText>().TriggerResolve(caps[activeCapIndex]);
        beltHandler.activeCanObject.GetComponent<ActiveCan>().canSprayRate = capSprayRate[activeCapIndex];
    }

    //set cap's particle style
    void SetCapParameters()
    {

        //sets individual parameters for each cap cycled to
        ParticleSystem par = canParticlesObj.GetComponent<ParticleSystem>();
        var main = par.main;
        var sh = par.shape;
        var em = par.emission;
        PaintIn3D.P3dPaintSphere paintSphere = canParticlesObj.GetComponent<PaintIn3D.P3dPaintSphere>();


        if (activeCap == "Stock")
        {
            main.startSpeed = 5;
            sh.angle = 4;
            em.rateOverTime = 300;
            paintSphere.Scale = new Vector3(0.2f, 0.2f, 0.2f);
        }
        if (activeCap == "Skinny")
        {
            main.startSpeed = 5;
            sh.angle = 1;
            em.rateOverTime = 400;
            paintSphere.Scale = new Vector3(0.1f, 0.1f, 0.1f);
        }
        if (activeCap == "N.Y. Fat")
        {
            main.startSpeed = 6;
            sh.angle = 8;
            em.rateOverTime = 500;
            paintSphere.Scale = new Vector3(0.25f, 0.25f, 0.25f);
        }
        if (activeCap == "Firehose")
        {
            main.startSpeed = 12;
            sh.angle = 20;
            em.rateOverTime = 200;
            paintSphere.Scale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        if (canPaintIsLow)
        {
            em.rateOverTime = em.rateOverTime.constant / 5;
        }


    }
}
