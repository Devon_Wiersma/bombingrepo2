using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactSound : MonoBehaviour
{
    Rigidbody rigidBody;
    AudioSource audioSource;

    public AudioClip audioClip;
    public float forceRequiredForSound = 10;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioClip = audioSource.clip;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!audioSource.isPlaying)
        {
            float colForce = collision.impulse.magnitude / Time.fixedDeltaTime;
         //   print(colForce);
            if (colForce > forceRequiredForSound)
            {
                audioSource.pitch = Random.Range(0.8f, 1.2f);
                audioSource.PlayOneShot(audioClip);
            }
        }

    }
}
