using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Michsky.UI.ModernUIPack;

public class LangText : MonoBehaviour
{
    public string Identifier;
    public float defaultFontSize;
    public float fontScaler = 0;

    public TextMeshProUGUI textComponent;

    private void Start()
    {
        textComponent = GetComponent<TextMeshProUGUI>();
        if (GetComponent<TextMeshProUGUI>() != null)
        {
            defaultFontSize = textComponent.fontSize;
            print("Default Font Size is: " + defaultFontSize);
        }
    }

    public void ScaleText(SystemLanguage _language)
    {
        if (textComponent != null)
        {
            textComponent.fontSize = defaultFontSize + fontScaler;
        }
    }

    public void TriggerResolve(string newString)
    {
        //sets identifier to new inputted string
        //and replaces gaps in text to fit string parameters
        Identifier = newString.Replace(" ", "");

        print("New Identifier is: " + Identifier);

        if (Identifier == "")
        {
            Debug.LogError("You are missing an identifier for your localization text! This string will not be translated");
        }

        //runs resolver
        if (FindObjectOfType<LangResolver>() != null)
        {
            FindObjectOfType<LangResolver>().ResolveTexts();
        }
        else
        {
            Debug.LogWarning("There is no Language Resolver present in the scene. Please add one somehow so nothing breaks");
        }

    }
}
