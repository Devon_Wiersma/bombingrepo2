using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltHandler : MonoBehaviour
{

    public GameObject activeBagObject;
    public GameObject activePenObject;
    public GameObject activeRollerObject;
    public GameObject placedBagObject;
    public GameObject playerObject;
    public GameObject activeItemHolderObject;
    GameObject activeItem;
    PauseMenu pauseMenu;
    Manager manager;

    public GameObject activeCanObject;
    public GameObject activeCanObjectModel;
    public GameObject activeCanParticleObject;
    public GameObject activeCanCapPointerObject;
    public GameObject capText;
    public GameObject penText;
    public GameObject rollerText;

    public GameObject activePenTip;
    public GameObject activeRollerTip;

    public Color[] canColors;
    public string[] canColorNames;
    public Material[] canMaterials;
    Material canDisplayMaterial;

    public bool canActive = false;
    public bool bagActive = true;
    public bool penActive = false;
    public bool itemActive = false;
    public bool rollerActive = false;

    bool clickReleased = true;
    bool objectInteractedWith = false;



    public Camera camera;

    RaycastHit hit;
    public float raycastLength;



    private void Awake()
    {
        penText.GetComponent<LangText>().Identifier = canColorNames[0];
        capText.GetComponent<LangText>().Identifier = canColorNames[0];
    }

    private void Start()
    {
        pauseMenu = GameObject.Find("Pause Menu Holder").GetComponent<PauseMenu>();
    }

    private void Update()
    {
        if (!pauseMenu.pauseVisible)
        {
            if (Input.GetMouseButton(0))
            {
                CheckClickRay();
                if (!objectInteractedWith)
                {
                    activeCanObject.GetComponent<PaintIn3D.P3dToggleParticles>().StartSpray();
                }
                if (objectInteractedWith)
                {
                    activeCanObject.GetComponent<PaintIn3D.P3dToggleParticles>().StopSpray();
                }
                clickReleased = false;
            }
            if (Input.GetMouseButtonUp(0))
            {
                //disable spray to prevent spraying the can on click
                activeCanObject.GetComponent<PaintIn3D.P3dToggleParticles>().StopSpray();
                activePenObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = true;
                activeRollerObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = true;
                clickReleased = true;
                objectInteractedWith = false;
            }

            //disable all other components to prevent overlap
            if (!itemActive)
            {
                if (bagActive)
                {
                    activeCanObject.SetActive(false);
                    activePenObject.SetActive(false);
                    activeBagObject.SetActive(true);
                    activeRollerObject.SetActive(false);
                }
                else
                {
                    activeBagObject.SetActive(false);
                }

                if (canActive)
                {
                    //  activeCanObject.GetComponent<PaintIn3D.P3dToggleParticles>().enabled = true;
                    activeBagObject.SetActive(false);
                    activePenObject.SetActive(false);
                    activeCanObject.SetActive(true);
                    activeRollerObject.SetActive(false);
                }
                else
                {
                    activeCanObject.SetActive(false);
                }

                if (penActive)
                {
                    //   activePenObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = true;
                    activeBagObject.SetActive(false);
                    activeCanObject.SetActive(false);
                    activePenObject.SetActive(true);
                    activeRollerObject.SetActive(false);
                }
                else
                {
                    //     activePenObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = false;
                    activePenObject.SetActive(false);
                }

                if (rollerActive)
                {
                    //   activePenObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = true;
                    activeBagObject.SetActive(false);
                    activeCanObject.SetActive(false);
                    activePenObject.SetActive(false);
                    activeRollerObject.SetActive(true);
                }
                else
                {
                    //     activePenObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = false;
                    activeRollerObject.SetActive(false);
                }
            }

            if (itemActive)
            {
                activeBagObject.SetActive(false);
                activeCanObject.SetActive(false);
                activePenObject.SetActive(false);
                activeRollerObject.SetActive(false);
            }
        }
    }


    void CheckClickRay()
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, raycastLength))
        {
            //gather hitpoint and adjust can position to accomodate 
            Vector3 aimPoint = hit.point;
            aimPoint = new Vector3(aimPoint.x, aimPoint.y, aimPoint.z);

            //rotates the cap object to face the destination
            //and the can itself to rotate while constrained on the Y axis to prevent spinning and maintain the can rotating effect

            //activeCanCapPointerObject.transform.LookAt(ray.direction);

            if (hit.point != null)
            {
                activeCanCapPointerObject.transform.LookAt(aimPoint);
                print(hit.collider.gameObject.name);
            }
            else
            {
                activeCanObject.transform.LookAt(ray.direction);
                activePenObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = false;
                activeRollerObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = false;
            }

            if (clickReleased && !objectInteractedWith)
            {
                if (bagActive)
                {
                    if (hit.collider.gameObject.tag == "Floor")
                    {
                        //disable spray to prevent double-placement
                        PlaceBag(hit.point);

                        objectInteractedWith = true;
                    }
                }
                else
                if (hit.collider.gameObject.tag == "Bag")
                {
                    TakeBag();
                    objectInteractedWith = true;
                }
                else
                if (hit.collider.gameObject.tag == "PaintCan")
                {
                    objectInteractedWith = true;
                    ChooseCanColour();

                }
                else
                if (hit.collider.gameObject.tag == "Pen")
                {
                    activePenObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = false;
                    ChoosePenColor();

                    objectInteractedWith = true;
                }
                else
                if (hit.collider.gameObject.tag == "Roller")
                {
                    activeRollerObject.GetComponent<PaintIn3D.P3dHitScreen>().enabled = false;
                    ChooseRollerColor();

                    objectInteractedWith = true;
                }
                else
                if ((hit.collider.gameObject.tag == "PlaceableSurface" || hit.collider.gameObject.tag == "Floor") && activeItem != null)
                {
                    //place object if surface faces up
                    if (hit.normal.y > 0.8)
                    {

                        ItemPlace(hit);
                    }
                    print(hit.normal);
                    objectInteractedWith = true;
                }
                else
                if (hit.collider.gameObject.tag == "Pickupable")
                {
                    //pickup item if none is held
                    if (activeItem == null)
                    {
                        ItemPickup(hit);
                        objectInteractedWith = true;
                    }
                }

                //BoomBoxButton Checker
                if (hit.collider.gameObject.name == "RandomButton")
                {
                    objectInteractedWith = true;
                    GameObject.Find("BoomBoxAudioSource").GetComponent<BoomBox>().NextSong();
                }
                if (hit.collider.gameObject.name == "OnSwitch")
                {
                    objectInteractedWith = true;
                    GameObject.Find("BoomBoxAudioSource").GetComponent<BoomBox>().ToggleBoombox();
                }
            }
        }
    }

    void ChooseRollerColor()
    {
        bagActive = false;
        penActive = false;
        canActive = false;
        rollerActive = true;

        activeCanObject.GetComponent<PaintIn3D.P3dToggleParticles>().enabled = false;

        int colorIndex = 0;

        //GREEN Can
        if (hit.collider.gameObject.name == "GreenRoller")
        {
            colorIndex = 11;
        }
        //NEON GREEN Can
        if (hit.collider.gameObject.name == "NGreenRoller")
        {
            colorIndex = 0;
        }
        //RED Can
        if (hit.collider.gameObject.name == "RedRoller")
        {
            colorIndex = 14;
        }
        //NEON RED Can
        if (hit.collider.gameObject.name == "NRedRoller")
        {
            colorIndex = 1;
        }
        //BLUE Can
        if (hit.collider.gameObject.name == "BlueRoller")
        {
            colorIndex = 15;
        }
        //NEON BLUE Can
        if (hit.collider.gameObject.name == "NBlueRoller")
        {
            colorIndex = 2;
        }
        //PINK Can
        if (hit.collider.gameObject.name == "PinkRoller")
        {
            colorIndex = 3;
        }
        //ORANGE Can
        if (hit.collider.gameObject.name == "OrangeRoller")
        {
            colorIndex = 13;
        }
        //NEON ORANGE Can
        if (hit.collider.gameObject.name == "NOrangeRoller")
        {
            colorIndex = 4;
        }
        //YELLOW Can
        if (hit.collider.gameObject.name == "YellowRoller")
        {
            colorIndex = 12;
        }
        //NEON YELLOW Can
        if (hit.collider.gameObject.name == "NYellowRoller")
        {
            colorIndex = 5;
        }
        //PURPLE Can
        if (hit.collider.gameObject.name == "PurpleRoller")
        {
            colorIndex = 6;
        }
        //BLACK Can
        if (hit.collider.gameObject.name == "BlackRoller")
        {
            colorIndex = 7;
        }
        //WHITE Can
        if (hit.collider.gameObject.name == "WhiteRoller")
        {
            colorIndex = 8;
        }
        //BROWN Can
        if (hit.collider.gameObject.name == "BrownRoller")
        {
            colorIndex = 9;
        }
        //GREY Can
        if (hit.collider.gameObject.name == "GreyRoller")
        {
            colorIndex = 10;
        }
        activeRollerTip.GetComponent<MeshRenderer>().material.color = canColors[colorIndex];
        activeRollerObject.GetComponent<PaintIn3D.P3dPaintDecal>().color = canColors[colorIndex];
        rollerText.GetComponent<FadeText>().DisplayText(canColorNames[colorIndex], canColors[colorIndex]);
        rollerText.GetComponent<LangText>().TriggerResolve(canColorNames[colorIndex]);

    }

    //Pen Colour
    void ChoosePenColor()
    {
        bagActive = false;
        penActive = true;
        canActive = false;
        rollerActive = false;

        activeCanObject.GetComponent<PaintIn3D.P3dToggleParticles>().enabled = false;

        int colorIndex = 0;

        if (hit.collider.gameObject.name == "GreenPen")
        {
            colorIndex = 0;
        }
        if (hit.collider.gameObject.name == "RedPen")
        {
            colorIndex = 1;
        }
        if (hit.collider.gameObject.name == "OrangePen")
        {
            colorIndex = 4;
        }
        if (hit.collider.gameObject.name == "YellowPen")
        {
            colorIndex = 5;
        }
        if (hit.collider.gameObject.name == "PurplePen")
        {
            colorIndex = 6;
        }
        if (hit.collider.gameObject.name == "BlackPen")
        {
            colorIndex = 7;
        }
        if (hit.collider.gameObject.name == "WhitePen")
        {
            colorIndex = 8;
        }
        if (hit.collider.gameObject.name == "BrownPen")
        {
            colorIndex = 9;
        }
        if (hit.collider.gameObject.name == "GreyPen")
        {
            colorIndex = 10;
        }
        if (hit.collider.gameObject.name == "BluePen")
        {
            colorIndex = 15;
        }
        if (hit.collider.gameObject.name == "PinkPen")
        {
            colorIndex = 3;
        }
        activePenTip.GetComponent<MeshRenderer>().material.color = canColors[colorIndex];
        activePenObject.GetComponent<PaintIn3D.P3dPaintDecal>().color = canColors[colorIndex];
        penText.GetComponent<FadeText>().DisplayText(canColorNames[colorIndex], canColors[colorIndex]);
        penText.GetComponent<LangText>().TriggerResolve(canColorNames[colorIndex]);

    }


    //Can COlour
    void ChooseCanColour()
    {
        bagActive = false;
        penActive = false;
        rollerActive = false;
        canActive = true;

        activeCanObject.GetComponent<PaintIn3D.P3dToggleParticles>().StopSpray();
        activeCanObject.GetComponent<ActiveCan>().canCapacity = 100;
        int colourIndex = 0;

        //Get the particle system
        ParticleSystem ps = activeCanParticleObject.GetComponent<ParticleSystem>();
        var main = ps.main;

        //GREEN Can
        if (hit.collider.gameObject.name == "GreenCan")
        {
            colourIndex = 11;
        }
        //NEON GREEN Can
        if (hit.collider.gameObject.name == "NGreenCan")
        {
            colourIndex = 0;
        }
        //RED Can
        if (hit.collider.gameObject.name == "RedCan")
        {
            colourIndex = 14;
        }
        //NEON RED Can
        if (hit.collider.gameObject.name == "NRedCan")
        {
            colourIndex = 1;
        }
        //BLUE Can
        if (hit.collider.gameObject.name == "BlueCan")
        {
            colourIndex = 15;
        }
        //NEON BLUE Can
        if (hit.collider.gameObject.name == "NBlueCan")
        {
            colourIndex = 2;
        }
        //PINK Can
        if (hit.collider.gameObject.name == "PinkCan")
        {
            colourIndex = 3;
        }
        //ORANGE Can
        if (hit.collider.gameObject.name == "OrangeCan")
        {
            colourIndex = 13;
        }
        //NEON ORANGE Can
        if (hit.collider.gameObject.name == "NOrangeCan")
        {
            colourIndex = 4;
        }
        //YELLOW Can
        if (hit.collider.gameObject.name == "YellowCan")
        {
            colourIndex = 12;
        }
        //NEON YELLOW Can
        if (hit.collider.gameObject.name == "NYellowCan")
        {
            colourIndex = 5;
        }
        //PURPLE Can
        if (hit.collider.gameObject.name == "PurpleCan")
        {
            colourIndex = 6;
        }
        //BLACK Can
        if (hit.collider.gameObject.name == "BlackCan")
        {
            colourIndex = 7;
        }
        //WHITE Can
        if (hit.collider.gameObject.name == "WhiteCan")
        {
            colourIndex = 8;
        }
        //BROWN Can
        if (hit.collider.gameObject.name == "BrownCan")
        {
            colourIndex = 9;
        }
        //GREY Can
        if (hit.collider.gameObject.name == "GreyCan")
        {
            colourIndex = 10;
        }

        activeCanObject.transform.rotation = new Quaternion(0, 0, 0, 0);
        main.startColor = canColors[colourIndex];
        activeCanParticleObject.GetComponent<PaintIn3D.P3dPaintSphere>().Color = canColors[colourIndex];
        activeCanObjectModel.GetComponent<MeshRenderer>().material.color = canColors[colourIndex];

        capText.GetComponent<FadeText>().DisplayText(canColorNames[colourIndex], canColors[colourIndex]);
        capText.GetComponent<LangText>().TriggerResolve(canColorNames[colourIndex]);
        activeCanObject.SetActive(true);
        activeCanObject.GetComponent<ActiveCan>().ResetCan();
    }

    //turn off bag and turn on can, placing it at the position of the click
    void PlaceBag(Vector3 bagPlacedPoint)
    {
        bagActive = false;
        canActive = false;
        penActive = false;
        rollerActive = false;

        //place item facing player and do some conversions
        //converts Quaternions to Euler and zeroes out the X to make the object stay grounded
        placedBagObject.transform.position = bagPlacedPoint;
        Vector3 dir = (playerObject.transform.position - placedBagObject.transform.position).normalized;
        Quaternion rotation = Quaternion.LookRotation(dir, hit.normal);
        Vector3 eulerRotation = new Vector3(0, rotation.eulerAngles.y, rotation.eulerAngles.z);
        placedBagObject.transform.up = hit.normal;
        placedBagObject.transform.forward = dir;
        placedBagObject.transform.localRotation = Quaternion.Euler(eulerRotation);



    }

    //pickup bag
    void TakeBag()
    {
        activeCanObject.GetComponent<PaintIn3D.P3dToggleParticles>().enabled = false;
        bagActive = true;
        canActive = false;
        penActive = false;
        rollerActive = false;
        placedBagObject.transform.position = new Vector3(-100, -100, -100);
    }

    //pickup item
    void ItemPickup(RaycastHit hit)
    {
        //put item in hand
        activeItem = hit.collider.gameObject;
        activeItem.transform.parent = activeItemHolderObject.transform;
        activeItem.transform.localPosition = Vector3.zero;
        activeItem.transform.rotation = activeItemHolderObject.transform.rotation;
        activeItem.transform.LookAt(transform.up);

        //disable any colliders on main object
        activeItem.GetComponent<Collider>().enabled = false;
        if (activeItem.GetComponent<Rigidbody>() != null)
            activeItem.GetComponent<Rigidbody>().isKinematic = true;
        if (hit.collider.gameObject.name == "Flashlight")
            activeItem.transform.localEulerAngles = new Vector3(-90, 90, 0);

        //disable physics components
        Collider[] collidersToDisable = activeItem.GetComponentsInChildren<Collider>();
        foreach (Collider collider in collidersToDisable)
        {
            if (collider != null)
                collider.enabled = false;
        }
        Rigidbody[] rigids = activeItem.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rigid in rigids)
        {
            if (rigid != null)
                rigid.isKinematic = true;
        }

        itemActive = true;
    }

    //place item on ground
    void ItemPlace(RaycastHit hit)
    {
        activeItem.transform.position = new Vector3(hit.point.x, hit.point.y + 0.25f, hit.point.z);
        activeItem.transform.parent = null;

        //disable physics components where applicable
        Collider[] collidersToDisable = activeItem.GetComponentsInChildren<Collider>();
        foreach (Collider collider in collidersToDisable)
        {
            collider.enabled = true;
        }
        Rigidbody[] rigids = activeItem.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rigid in rigids)
        {
            if (rigid != null)
                rigid.isKinematic = true;

            if (activeItem.name == "Flashlight")
            {
                rigid.isKinematic = false;
            }
        }

        //place item facing player and do some conversions
        //converts Quaternions to Euler and zeroes out the X to make the object stay grounded
        activeItem.transform.position = hit.point;
        Vector3 dir = (playerObject.transform.position - activeItem.transform.position).normalized;
        Quaternion rotation = Quaternion.LookRotation(dir, this.transform.up);
        Vector3 eulerRotation = new Vector3(0, rotation.eulerAngles.y, rotation.eulerAngles.z);
        if (activeItem.name == "Flashlight")
        {
            activeItem.transform.position += Vector3.up / 3;
        }
        else
        {
            activeItem.transform.rotation = Quaternion.Euler(eulerRotation);
        }


        activeItem = null;
        itemActive = false;
    }

}