using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName
= "GameData")]

public class ScriptableGameData : ScriptableObject
{


    [SerializeField]
    public static bool clearSaveDataInLevel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void EnableLevelDeletion(int levelName)
    {
        clearSaveDataInLevel = true;
    }

}
