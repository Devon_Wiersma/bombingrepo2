﻿using Steamworks;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    private bool loadScene = false;

    public GameObject[] objectsToDisable;
    public GameObject[] objectsToEnable;

    public int sceneToLoad;
    public Text loadingTx;

    public void Start()
    {
        if (!SteamManager.Initialized)
            return;

        CheckAchievementLoad();

    }

    public void ReloadScene()
    {
        Time.timeScale = 1;
        GameObject pauseMenu = GameObject.Find("Pause Menu Holder");
        pauseMenu.SetActive(false);
        StartCoroutine(LoadNewScene(SceneManager.GetActiveScene().buildIndex));
    }


    // Updates once per frame
    public void LoadScene(int sceneIndexToLoad)
    {
        Time.timeScale = 1;
        GameObject pauseMenu = GameObject.Find("Pause Menu Holder");
        if (pauseMenu != null)
            pauseMenu.SetActive(false);

        // If the player has pressed the space bar and a new scene is not loading yet...
        if (loadScene == false)
        {

            // ...set the loadScene boolean to true to prevent loading a new scene more than once...
            loadScene = true;

            // ...change the instruction text to read "Loading..."
            loadingTx.enabled = true;

            // ...and start a coroutine that will load the desired scene.
            StartCoroutine(LoadNewScene(sceneIndexToLoad));

        }

        // If the new scene has started loading...
        if (loadScene == true)
        {

            // ...then pulse the transparency of the loading text to let the player know that the computer is still working.
            //loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));

        }

    }


    // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    IEnumerator LoadNewScene(int sceneToLoad)
    {
        for (int i = 0; i < objectsToDisable.Length; i++)
        {
            if (objectsToDisable[i] != null)
                objectsToDisable[i].SetActive(false);
        }

        for (int i = 0; i < objectsToEnable.Length; i++)
        {
            if (objectsToEnable[i] != null)
                objectsToEnable[i].SetActive(true);
        }


        // This line waits for 3 seconds before executing the next line in the coroutine.
        // This line is only necessary for this demo. The scenes are so simple that they load too fast to read the "Loading..." text.
        yield return new WaitForSeconds(3);

        // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneToLoad);

        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (!async.isDone)
        {
            yield return null;
        }
    }


    void CheckAchievementLoad()
    {
        //check if playerprefs has already stored and achievement for this level number
        //and if not, give the achievement
        if (!PlayerPrefs.HasKey("LoadedLevel" + SceneManager.GetActiveScene().buildIndex))
        {
            SteamUserStats.SetAchievement("ACHIEVEMENT_STARTEDLEVEL_" + SceneManager.GetActiveScene().buildIndex);
            SteamUserStats.StoreStats();
            PlayerPrefs.SetString("LoadedLevel" + SceneManager.GetActiveScene().buildIndex, SceneManager.GetActiveScene().buildIndex.ToString());
            PlayerPrefs.Save();
            print("LoadedLevel" + SceneManager.GetActiveScene().buildIndex + " Saved to PlayerPrefs");
            print("ACHIEVEMENT_STARTEDLEVEL_" + SceneManager.GetActiveScene().buildIndex + " added as achievement");
        }
        else
        {
            print("Player Loaded Level Previously, no achievements");
        }
    }

}
