using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowLights : MonoBehaviour
{
    public Material lightOnMaterial;
    public Material lightOffMaterial;

    bool lightIsOn;

    // Start is called before the first frame update
    void Start()
    {
        if (Random.Range(0, 10) >= 5)
        {
            lightIsOn = true;
        }
        else
        {
            lightIsOn = false;
        }
        StartCoroutine("WaitForLightSwitch");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator WaitForLightSwitch()
    {
        yield return new WaitForSeconds(Random.Range(15, 60));
        if (lightIsOn)
        {
            GetComponent<MeshRenderer>().material = lightOffMaterial;
            lightIsOn = false;
            print("Light turned off");
        }
        else
        {
            GetComponent<MeshRenderer>().material = lightOnMaterial;
            lightIsOn = true;
            print("Light turned on");
        }

        StartCoroutine("WaitForLightSwitch");
    }
}
