using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomImageViewer : MonoBehaviour
{
    public Sprite[] imagesToDisplay;

    private void OnEnable()
    {
        GetComponent<Image>().sprite = imagesToDisplay[Random.Range(0, imagesToDisplay.Length)];
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
