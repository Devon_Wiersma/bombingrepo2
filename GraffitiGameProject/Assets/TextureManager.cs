using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;

[ExecuteAlways]

public class TextureManager : MonoBehaviour
{
    public P3dPaintableTexture[] paintableObjects;
    string currentScene;
    string filePath;

    public bool deleteSaves;

    TextureManager instance;


    // Start is called before the first frame update
    void Awake()
    {
        //generates list of objects on load
        paintableObjects = GameObject.FindObjectsOfType<P3dPaintableTexture>();

        if (Application.IsPlaying(gameObject))
        {
            //checks boolean of persistent lang resolver and toggles it if the player is deleting the saved data
            deleteSaves = GameObject.FindObjectOfType<LangResolver>().deleteSaveData;
            GameObject.FindObjectOfType<LangResolver>().deleteSaveData = false;

            currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

            //iterate through all paintable objects
            for (int i = 0; i < paintableObjects.Length; i++)
            {

                P3dPaintableTexture paintable = paintableObjects[i];
                paintable.SaveName = (currentScene + "-" + i);

                //removes saves in playerprefs if LAngResolver had a true bool (set in main menu)
                if (deleteSaves)
                {
                    paintable.ClearSave();
                }

                // print("Loaded texture from " + paintable.SaveName);
            }
            deleteSaves = false;
        }
    }



    private void OnDestroy()
    {
        if (Application.IsPlaying(gameObject))
        {
            // paintableObjects = GameObject.FindObjectsOfType<P3dPaintableTexture>();
            for (int i = 0; i < paintableObjects.Length; i++)
            {
                P3dPaintableTexture paintable = paintableObjects[i];

                paintable.SaveName = (currentScene + "-" + i);

                //print("Item index " + (currentScene + "-" + i.ToString()) + " has been saved!");

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Application.IsPlaying(gameObject))
        {
            paintableObjects = GameObject.FindObjectsOfType<P3dPaintableTexture>();
        }


    }

    void SaveData(string saveName, Texture2D texture)
    {
        byte[] _bytes = texture.EncodeToPNG();


        if (!System.IO.Directory.Exists(filePath))
        {
            System.IO.Directory.CreateDirectory(filePath);
        }
        File.WriteAllBytes(filePath + saveName + ".png", _bytes);
        print("Item saved at " + filePath + saveName + ".png" + " has been saved!");
    }

    static Texture2D LoadData(string saveName, Texture2D texture)
    {
        string filePath = Application.persistentDataPath;


        Texture2D tex = null;

        if (File.Exists(filePath + saveName + ".png"))
        {
            File.ReadAllBytes(filePath);

            byte[] _bytes = texture.EncodeToPNG();
            tex = new Texture2D(2, 2);
            tex.LoadImage(_bytes);
            print("Item saved at " + filePath + saveName + ".png" + " has been loaded!");
            return tex;
        }
        print("Item failed to load");
        return null;




    }
}
